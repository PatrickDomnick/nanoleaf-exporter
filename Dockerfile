FROM scratch
LABEL maintainer="patrickfdomnick@gmail.com"

# Copy from Dist
ARG PLATFORM
COPY dist/${PLATFORM}/nanoleaf-exporter /

# Expose Port and run Exporter
EXPOSE 8080/tcp
ENTRYPOINT ["/nanoleaf-exporter"]
