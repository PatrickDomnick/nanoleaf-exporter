module gitlab.com/PatrickDomnick/nanoleaf-exporter

go 1.16

require (
	github.com/prometheus/client_golang v1.11.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.10.1
	golang.org/x/time v0.0.0-20211116232009-f0f3c7e86c11
)
