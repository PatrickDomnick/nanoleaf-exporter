package nanoleaf

import (
	log "github.com/sirupsen/logrus"
)

type NanoleafController struct {
	Name            string `json:"name"`
	SerialNo        string `json:"serialNo"`
	Manufacturer    string `json:"manufacturer"`
	FirmwareVersion string `json:"firmwareVersion"`
	HardwareVersion string `json:"hardwareVersion"`
	Model           string `json:"model"`
	CloudHash       struct {
	} `json:"cloudHash"`
	Discovery struct {
	} `json:"discovery"`
	Effects struct {
		EffectsList []string `json:"effectsList"`
		Select      string   `json:"select"`
	} `json:"effects"`
	FirmwareUpgrade struct {
	} `json:"firmwareUpgrade"`
	PanelLayout struct {
		GlobalOrientation struct {
			Value int `json:"value"`
			Max   int `json:"max"`
			Min   int `json:"min"`
		} `json:"globalOrientation"`
		Layout struct {
			NumPanels    int `json:"numPanels"`
			SideLength   int `json:"sideLength"`
			PositionData []struct {
				PanelID   int `json:"panelId"`
				X         int `json:"x"`
				Y         int `json:"y"`
				O         int `json:"o"`
				ShapeType int `json:"shapeType"`
			} `json:"positionData"`
		} `json:"layout"`
	} `json:"panelLayout"`
	Rhythm struct {
		AuxAvailable    bool   `json:"auxAvailable"`
		FirmwareVersion string `json:"firmwareVersion"`
		HardwareVersion string `json:"hardwareVersion"`
		RhythmActive    bool   `json:"rhythmActive"`
		RhythmConnected bool   `json:"rhythmConnected"`
		RhythmID        int    `json:"rhythmId"`
		RhythmMode      int    `json:"rhythmMode"`
		RhythmPos       struct {
			X float64 `json:"x"`
			Y float64 `json:"y"`
			O float64 `json:"o"`
		} `json:"rhythmPos"`
	} `json:"rhythm"`
	Schedules struct {
	} `json:"schedules"`
	State struct {
		Brightness struct {
			Value int `json:"value"`
			Max   int `json:"max"`
			Min   int `json:"min"`
		} `json:"brightness"`
		ColorMode string `json:"colorMode"`
		Ct        struct {
			Value int `json:"value"`
			Max   int `json:"max"`
			Min   int `json:"min"`
		} `json:"ct"`
		Hue struct {
			Value int `json:"value"`
			Max   int `json:"max"`
			Min   int `json:"min"`
		} `json:"hue"`
		On struct {
			Value bool `json:"value"`
		} `json:"on"`
		Sat struct {
			Value int `json:"value"`
			Max   int `json:"max"`
			Min   int `json:"min"`
		} `json:"sat"`
	} `json:"state"`
}

func GetController() NanoleafController {
	// Devices Endpoint
	log.Info("Getting Controller Information")

	controllerApiResponse := NanoleafController{}

	// Execute the Bosch API Call
	err := getJSON(&controllerApiResponse)

	// HTTP Error
	if err != nil {
		log.Fatal(err.Error())
	}

	return controllerApiResponse
}
