package collector

import (
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/PatrickDomnick/nanoleaf-exporter/pkg/nanoleaf"
)

type NanoleafController struct {
	On               *prometheus.Desc
	Brightness       *prometheus.Desc
	Hue              *prometheus.Desc
	Saturation       *prometheus.Desc
	ColorTemperature *prometheus.Desc
}

func NewNanoleafController() *NanoleafController {
	return &NanoleafController{
		On:               prometheus.NewDesc(onState, "On State of the Nanoleaf Device", defaultLabels, nil),
		Brightness:       prometheus.NewDesc(brightness, "Brightness percentage of the Nanoleaf Device", defaultLabels, nil),
		Hue:              prometheus.NewDesc(hue, "Hue Degree of the Nanoleaf Device", defaultLabels, nil),
		Saturation:       prometheus.NewDesc(saturation, "Saturation percentage of the Nanoleaf Device", defaultLabels, nil),
		ColorTemperature: prometheus.NewDesc(colorTemperature, "Color temperature of the Nanoleaf Device", defaultLabels, nil),
	}
}

func (s *NanoleafController) Describe(c chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(s, c)
}

func (s *NanoleafController) Collect(c chan<- prometheus.Metric) {
	log.Info("Collecting Metrics for Device Power")
	nc := nanoleaf.GetController()
	// Preapre Boolean Values
	on := 0
	if nc.State.On.Value {
		on = 1
	}
	// Apply Values
	c <- prometheus.MustNewConstMetric(s.On, prometheus.GaugeValue, float64(on), nc.Name, nc.SerialNo, nc.Manufacturer, nc.FirmwareVersion, nc.HardwareVersion, nc.Model)
	c <- prometheus.MustNewConstMetric(s.Brightness, prometheus.GaugeValue, float64(nc.State.Brightness.Value), nc.Name, nc.SerialNo, nc.Manufacturer, nc.FirmwareVersion, nc.HardwareVersion, nc.Model)
	c <- prometheus.MustNewConstMetric(s.Hue, prometheus.GaugeValue, float64(nc.State.Hue.Value), nc.Name, nc.SerialNo, nc.Manufacturer, nc.FirmwareVersion, nc.HardwareVersion, nc.Model)
	c <- prometheus.MustNewConstMetric(s.Saturation, prometheus.GaugeValue, float64(nc.State.Sat.Value), nc.Name, nc.SerialNo, nc.Manufacturer, nc.FirmwareVersion, nc.HardwareVersion, nc.Model)
	c <- prometheus.MustNewConstMetric(s.ColorTemperature, prometheus.GaugeValue, float64(nc.State.Ct.Value), nc.Name, nc.SerialNo, nc.Manufacturer, nc.FirmwareVersion, nc.HardwareVersion, nc.Model)
}
