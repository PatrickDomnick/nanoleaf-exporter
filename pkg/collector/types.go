package collector

const onState = "nanoleaf_on"
const brightness = "nanoleaf_brightness"
const hue = "nanoleaf_hue"
const saturation = "nanoleaf_saturation"
const colorTemperature = "nanoleaf_color"

var defaultLabels = []string{"name", "serialNo", "manufacturer", "firmwareVersion", "hardwareVersion", "model"}
